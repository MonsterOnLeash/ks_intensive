def date(day, month, year):
    if month in [1, 3, 5, 7, 8, 10, 12] and 1 <= day <= 31:
        return True
    if month in [4, 6, 9, 11] and 1 <= day <= 30:
        return True
    if month == 2:
        if 1 <= day <= 28:
            return True
        if day == 29:
            if (year % 400) != 0:
                return True
            if (year % 100) == 0:
                return False
            if (year % 4) != 0:
                return True
    return False

