def sum_range(a,z):
    ans = 0
    if a > z:
        a, z = z, a
    for i in range(a, z + 1):
        ans += i
    return ans
