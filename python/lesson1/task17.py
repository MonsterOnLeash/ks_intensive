class Toy:
    definition = "an object for a child to play with, typically a model or miniature replica of something"

    def __init__(self, size, material, name):
        self.size = size
        self.material = material
        self.name = name

teddyBear = Toy('medium-sized', 'fur', 'Whinnie-the-Pooh')
boat = Toy('small', 'metal', 'Titanic')
doll = Toy("'small", 'plastic', 'Barbie')