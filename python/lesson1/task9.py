def my_func(a):
    repeated = set()
    unique = []
    for i in a:
        if i not in repeated:
            unique.append(i)
            repeated.add(i)
    return tuple(reversed(unique))
