def Top3Keys(my_dict):
    s = sorted(list(my_dict.items()), key=lambda pair : pair[1], reverse=True)
    for i in range(min(3, len(s))):
        print(s[i][0], end = ' ')
    print()

