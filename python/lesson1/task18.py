class Book:
    definition = "a written or printed work consisting of pages glued or sewn together along one side and bound in covers"

    def __init__(self, size, author, name):
        self.size = size
        self.author = author
        self.name = name

    def __str__(self):
        return f"{self.name} is {self.size} and was written by {self.author}"

    def effect(self, feeling):
        return f"{self.name} is {feeling}"

class HorrorBook(Book):
    def effect(self, feeling = "terryfying"):
        return super().effect(feeling)

class DetectiveStory(Book):
    def effect(self, feeling = "intriguing"):
        return f"the story is {feeling}"
    def whoisthemurderer():
        return "butler"


class Dictionary(Book):
    def effect(feeling = "trustworthy"):
        return f"this dictionary is {feeling}"


