def funcCounter(func):
    def wrapper():
        wrapper.counter += 1
        funcOutput = func()
        print(func.__name__ + " has been called " + str(wrapper.counter) + " times")
        return funcOutput
    
    wrapper.counter = 0
    return wrapper


@funcCounter
def hello():
    print("Hello")
